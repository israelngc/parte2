resource "aws_cloudwatch_log_group" "container-custom-kotlin" {
  name              = "container-custom-kotlin"
  retention_in_days = 1
}

resource "aws_ecs_task_definition" "container-custom-kotlin" {
  family = "container-custom-kotlin"

  container_definitions = <<EOF
[
  {
    "name": "container-custom-kotlin",
    "image": "kotlin-israel",
    "cpu": 0,
    "memory": 128,
    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-region": "us-east-2",
        "awslogs-group": "container-custom-kotlin",
        "awslogs-stream-prefix": "complete-ecs"
      }
    }
  }
]
EOF
}

resource "aws_ecs_service" "container-custom-kotlin" {
  name = "container-custom-kotlin"
  cluster = var.cluster_id
  task_definition = aws_ecs_task_definition.hello_world.arn

  desired_count = 1

  deployment_maximum_percent = 10
  deployment_minimum_healthy_percent = 0
}
