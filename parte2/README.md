# # # PARTE 2


* Crie um diretório com nome de "parte2" e faça esses steps dentro dele.
* Provisione uma infraestrutura na AWS com terraform para subir essa aplicação containerizada
* Lembre-se de utilizar EC2, ELB, ASG entre outros serviços da AWS
* Crie um pipeline com GitLab CI para automatizar a execução destes passos e subir essa infraestrutura na AWS

